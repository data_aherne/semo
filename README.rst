====
SEMO Public API
====
A demo for SEMO reports available via SEMO's public API

:Author: Derek Aherne                                          
:Contact: derek.aherne@sse.com                                 
:Version: 1.0 30/03/2020

**Description**

All reports are read via the catelogue called from https://reports.sem-o.com/api/v1/documents/static-reports?. 
The catelogue details all available reports on the SEMO server. Using the catelogue explicit URIs are constructed to extract xml and parse to a data frame